<div class="container"><!--  Form container -->
  <p class="alert alert-primary" role="alert">Fill all fields</p>
   <form action ="Output.php" method="post" autocomplete="on">
    <div class="form-group">
      <input class="form-control" type="text"   name="userName"    placeholder="Name">
      <br>
      <input class="form-control" type="text"   name="userSurname" placeholder="Surname">
      <br>
      <input class="form-control" type="number" name="userAge"     placeholder="Age">
      <br>
    
      <select class="form-control" name="userGender" id="">
        <option value="">Gender</option>
        <option value="female">Female</option>
        <option value="male">Male</option>
      </select>
      <br>
        
      <div class="form-row">
        <div class="col-sm">
          <input class="form-control" type="number" name="userWeight" placeholder="weight">
        </div>

        <div class="col-sm">
          <select class="form-control" name="unit" id="">
            <option value="">Kg / Lbs</option>
            <option value="kg">Kg</option>
            <option value="lbs">Lbs</option>
          </select>
        </div>
      </div>
      
    </div>
    
    <input class="btn btn-primary btn-sm"  type="submit" name="submit">
  </form>
</div><!--  Form contatiner -->

