<?php include "header.php" ?>

<?php 
 
  $rider->checkFields();

  $rider->checkInput();
 
  $result = $rider->calculate(); 

?>


<!-- Outpout in HTML -->
<div class="container"><!--  Output contatiner -->
  <h4><?php echo $rider->name . "'s "?>estimated FTP </h4>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Rider: <?php echo $rider->name . " " . $rider->surname; ?></li>
    <li class="list-group-item">age: <?php echo $rider->age; ?></li>
    <li class="list-group-item">Gender: <?php echo $rider->gender; ?></li>
    <li class="list-group-item">Weight: <?php echo $rider->weighout . " " . $rider->weighunit; ?></li>
    <li class="list-group-item">Estimated FTP: <strong><?php echo $result; ?></strong></li>
  </ul>  

  <br>

  <div class="row justify-content-md-center"">
     <a class="btn btn btn-outline-secondary btn-sm" href="index.php"> back </a>                
  </div>
</div><!--  Output contatiner -->
<hr>

<?php include "footer.php" ?>