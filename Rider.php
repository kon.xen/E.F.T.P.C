<?php 

class Rider{

//---- Options ---- //

  public $name;
  public $surname;
  public $age;
  public $gender;
  public $weighunit;
  public $weighout;
  public $riderFtp;
  public $weightToCalc;
  public $error = '';
     
//---- Methods ---- //  

//Redirecting   
public static function redirect($location){
    return header("Location:" . $location);
}//

// creates instance $the_object
public static function instantiation(){  

    $calling_class = get_called_class();
    $the_object = new $calling_class;

    return $the_object;
}//

//all fields completed? 
public function checkFields(){

    //$items = array_count_values($_POST);
    $count = 0;

    foreach ($_POST as $key) {
        if (empty($key)){
            $count++;                
        }
    }
     
    if ($count >= 1){
        self::redirect('index.php');
    } 
 }//


// Check for input .... and asign to variables //
public function checkInput(){
     
    $this->name    = $_POST['userName'];
    $this->surname = $_POST['userSurname'];
    $this->age     = $_POST['userAge'];
    $this->gender  = $_POST['userGender'];

    //is it Lbs?
    if ($_POST['unit'] == "lbs"){
        $this->weightToCalc = $_POST['userWeight'];
        $this->weighout     = $_POST['userWeight'];
        $this->weighunit    = "Lbs";  
    }//

    // is it Kgs?
    if ($_POST['unit'] == "kg"){
        $this->weightToCalc = $_POST['userWeight'] * 2.2;     //converts Kg to lbs
        $this->weighout     = $_POST['userWeight'];
        $this->weighunit    = "Kg";
    }// 

 } //EOF CheckInput



// Check what calc to do.
   
public function calculate(){

      if ($this->gender == "male"){
        if ($this->age > 35){
            $ftpTemp        = $this->weightToCalc * 2;
            $ageDif         = ($this->age - 35) * 0.005;
            $ageModifier    = $ftpTemp * $ageDif;
            $this->riderFtp = $ftpTemp - $ageModifier;

            return  $this->riderFtp;
            unset($this->weightToCalc);

        } else {
            $this->riderFtp = $this->weightToCalc * 2;

            return  $this->riderFtp;
            unset($this->weightToCalc);
        }

      } elseif ($this->gender == "female"){
        if ($this->age > 35){
            $ftpTemp        = $this->weightToCalc * 2;
            $ageDif         = ($this->age - 35) * 0.005;
            $ageModifier    = $ftpTemp * $ageDif;
            $genderModifier = $ftpTemp * 0.1;
            $this->riderFtp = $ftpTemp - $ageModifier - $genderModifier;

            return  $this->riderFtp;
            unset($this->weightToCalc);

        } else {
            $ftpTemp  = $this->weightToCalc * 2;
            $this->riderFtp = $ftpTemp;

            return  $this->riderFtp;
            unset($this->weightToCalc);
        }  

    }//EOF female 


}// EOF Calculate


}// end of class Rider 

?>